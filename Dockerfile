FROM ghcr.io/home-assistant/home-assistant:stable

RUN apk add build-base git linux-headers py3-grpcio
RUN pip3 install --upgrade pip
RUN pip install yandex-speechkit